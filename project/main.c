/**
 * @file main.c
 * @author Puric Milovan 0207/2016
 * @date 2020
 * @brief SRV Project
 *
 * Project 57
 *
 * @version [1.0 @ 09/2020] Initial version
 */

/* Standard includes. */
#include <stdio.h>
#include <stdlib.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "queue.h"
#include "uart.h"

/* Hardware includes. */
#include "msp430.h"
#include "hal_ETF5438A.h"

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/** macro to convert ASCII code to digit */
#define ASCII2DIGIT(x)      (x - '0')
/** macro to convert digit to ASCII code */
#define DIGIT2ASCII(x)      (x + '0')

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/* Type for description of ADC channel number */
typedef enum
{
    CH0,    /** channel 0 */
    CH1,    /** channel 1 */
    CH2,    /** channel 2 */
    CH3     /** channel 3 */
} Channel_t;

/* Structure which describes message of queue "xADCDataQueue". */
typedef struct
{
    Channel_t eADCNr;       /** Number of channel from which values was read */
    uint16_t usADCData;     /** 12-bit result of AD conversion */
} ADCMsg_t;

/* Length of queue "xADCDataQueue" */
#define adcADC_DATA_QUEUE_LEN   ( 16 )

/* Macro for starting AD conversion */
#define adcSTART_CONV           do { ADC12CTL0 |= ADC12SC; } while( 0 )

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/* Type for description of source for message sent to queue "xQueueT2". */
typedef enum
{
    sourceUARTCallback,     /* message was sent from UART callback routine */
    sourceTask3             /* message was sent from xTask3 */
} Source_t;

/* Structure which describes message of queue "xQueueT2". */
typedef struct
{
    Source_t eTaskNr;           /** Source from message was sent */
    uint16_t usQueueT2Data;     /** Data of message (context depends of source of message) */
} QueueT2Msg_t;

/* Length of queue "xQueueT2" */
#define mainQUEUE_T2_LEN   ( 16 )

/* Struct for message from Task1 send to queue "xQueueMailbox" */
typedef struct
{
    uint16_t usAvgResult0;
    uint16_t usAvgResult1;
    uint16_t usAvgResult2;
    uint16_t usAvgResult3;
} QueueMailboxMsg_t;

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/* highest task priority */
#define mainHP_TASK_PRIO        ( 5 )
/* second task priority */
#define mainSP_TASK_PRIO        ( 4 )
/* third task priority */
#define mainTP_TASK_PRIO        ( 3 )
/* fourth task priority */
#define mainFP_TASK_PRIO        ( 2 )
/* led toggling task priority */
#define mainLED_TASK_PRIO       ( 1 )

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/* timer periods */
#define mainACVISITION_PERIOD     ( pdMS_TO_TICKS( 1000 ) )
#define mainTIMER_DEB_PERIOD     ( pdMS_TO_TICKS( 20 ) )

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/* Declarations of all tasks and functions used */

static void prvSetupHardware( void );
static void prvTaskTimer( void *pvParameters );
static void prvTask1( void *pvParameters );
static void prvTask2( void *pvParameters );
static void prvTask3( void *pvParameters );
static void prvTaskLEDs( void *pvParameters );

static void vLEDsUpdate(void);

static void vTimerDebounceCallback( TimerHandle_t xTimer );

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/* Task which processes results of AD conversion */
static TaskHandle_t xTask1;

/* binary semaphore declaration */
static SemaphoreHandle_t xSemaBDebounce = NULL;

/* Queue in which is, in ADC ISR, written results of AD conversion from each channel */
static QueueHandle_t xADCDataQueue = NULL;
/* Queue in which UART Callback routine and xTask3 send messages to xTask2 */
static QueueHandle_t xQueueT2 = NULL;
/* Queue in which Task1 write average resule */
static QueueHandle_t xQueueMailbox = NULL;

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/* Global variables used */

/* Threshold used for toggling LEDs */
static uint16_t usThreshold = 0;
/* Array that tells which LEDs are on/off */
static uint8_t ucStateOfLeds[4] = { 0 };
/* Global count for counting average value */
static uint8_t ucCount = 0;

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/**
 * @brief TaskTimer
 *
 * Task which start acvisition from AD convertor.
 */
static void prvTaskTimer( void *pvParameters )
{
    TickType_t xLastWakeTime;
    const TickType_t xPeriod = mainACVISITION_PERIOD;

    xLastWakeTime = xTaskGetTickCount();

    for ( ;; )
    {
        vTaskDelayUntil( &xLastWakeTime, xPeriod);

        adcSTART_CONV;

    }
}

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/**
 * @brief Task 1
 *
 * Task which offloads ADC ISR and calculates average of last 4 received results of AD conversions for each of 4 a channels of ADC
 */
static void prvTask1( void *pvParameters )
{
/* Local variable used to store received message from ADC ISR */
ADCMsg_t ADCReceived;
/* Local variable to buffer data fom QueueMailbox */
QueueMailboxMsg_t QueueMailboxMsg;

    for ( ;; )
    {
        /* Task waits for notification from ADC ISR */
        if ( ulTaskNotifyTake( pdTRUE, portMAX_DELAY ) > 0 )
        {
            /* Event received */
            if( ucCount == 4 )
            {
                QueueMailboxMsg.usAvgResult0 = 0;
                QueueMailboxMsg.usAvgResult1 = 0;
                QueueMailboxMsg.usAvgResult2 = 0;
                QueueMailboxMsg.usAvgResult3 = 0;
            }
            /* Reading data from queue xADCDataQueue */
            xQueueReceive( xADCDataQueue, &ADCReceived, portMAX_DELAY );
            QueueMailboxMsg.usAvgResult0 += ADCReceived.usADCData;

            xQueueReceive( xADCDataQueue, &ADCReceived, portMAX_DELAY );
            QueueMailboxMsg.usAvgResult1 += ADCReceived.usADCData;

            xQueueReceive( xADCDataQueue, &ADCReceived, portMAX_DELAY );
            QueueMailboxMsg.usAvgResult2 += ADCReceived.usADCData;

            xQueueReceive( xADCDataQueue, &ADCReceived, portMAX_DELAY );
            QueueMailboxMsg.usAvgResult3 += ADCReceived.usADCData;

            if( ucCount > 15 )
            {
                /* Divide all by 4 */
                QueueMailboxMsg.usAvgResult0 >>= 2;
                QueueMailboxMsg.usAvgResult1 >>= 2;
                QueueMailboxMsg.usAvgResult2 >>= 2;
                QueueMailboxMsg.usAvgResult3 >>= 2;
                ucCount = 0;

                xQueueOverwrite( xQueueMailbox, &QueueMailboxMsg);
            }
        }
    }
}

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/**
 * @brief Task 2
 *
 * Task which gets messages from Task 3 and UART and changes threshold depending of source of message in queue "xQueueT2".
 * If message was sent from UART then that value will become new threshold,
 * else if message was sent from Task 3 threshold is incremented/decremented (depending of button)
 * and current state of LEDs is sent via UART.
 */
static void prvTask2( void *pvParameters )
{
    /* buffer where data received from UART or Task3 is placed */
    QueueT2Msg_t xQueueT2Message;
    /* data being sent over UART */
    char data[30];

    for ( ;; )
    {
        /* task blocks waiting for message in queue */
        xQueueReceive( xQueueT2, &xQueueT2Message, portMAX_DELAY );

        if ( xQueueT2Message.eTaskNr == sourceUARTCallback )
        {
            usThreshold = xQueueT2Message.usQueueT2Data;
        }
        else if ( xQueueT2Message.eTaskNr == sourceTask3 )
        {
            if ( xQueueT2Message.usQueueT2Data == 1 )
            {
                /* button S1 is pressed */
                if ( usThreshold != 4095 )
                {
                    usThreshold = ( usThreshold + 1 ) & 4095;
                }
            }
            else if ( xQueueT2Message.usQueueT2Data == 2 )
            {
                /* button S2 is pressed */
                if ( usThreshold != 0 )
                {
                    usThreshold = ( usThreshold - 1 ) & 4095;
                }
            }

            vLEDsUpdate();

            sprintf( data, "State of LEDs: %d%d%d%d\n", ucStateOfLeds[3], ucStateOfLeds[2], ucStateOfLeds[1], ucStateOfLeds[0] );
            xUartSendString( data, 0 );
        }
    }
}

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/**
 * @brief Task 3
 *
 * Task which debounces buttons S1 and S2 and notifies Task2 via queue "xQueueT2"
 */
static void prvTask3( void *pvParameters )
{
uint8_t ucLastStateS1 = 0, ucStateS1 = 0;
uint8_t ucLastStateS2 = 0, ucStateS2 = 0;

    for ( ;; )
    {
        xSemaphoreTake( xSemaBDebounce, portMAX_DELAY );

        /* read S1 */
        ucStateS1 = P2IN & BIT4;
        if( ( ucStateS1 == 0 ) && ( ucLastStateS1 == BIT4 ) )
        {
            QueueT2Msg_t xMsg = { sourceTask3, 1 };
            xQueueSendToBack( xQueueT2, &xMsg, portMAX_DELAY);
        }
        /* save state */
        ucLastStateS1 = ucStateS1;

        /* read S2 */
        ucStateS2 = P2IN & BIT5;
        if( ( ucStateS2 == 0x00 ) && ( ucLastStateS2 == BIT5 ) )
        {
            QueueT2Msg_t xMsg = { sourceTask3, 2 };
            xQueueSendToBack( xQueueT2, &xMsg, portMAX_DELAY);
        }
        /* save state */
        ucLastStateS2 = ucStateS2;
    }
}

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/**
 * @brief Task LEDs task
 *
 * Task which turns LEDs on/off
 */
static void prvTaskLEDs( void *pvParameters )
{
    for ( ;; )
    {
        vLEDsUpdate();
    }
}

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/**
 * @brief Function vLEDsUpdate
 *
 * Function which updates state of LEDs depending of threshold
 */
static void vLEDsUpdate(void)
{
   QueueMailboxMsg_t QueueMailboxMsg;
   xQueuePeek( xQueueMailbox, &QueueMailboxMsg, portMAX_DELAY );
    if ( QueueMailboxMsg.usAvgResult0 < usThreshold )
    {
        halSET_LED( LED1 );
        ucStateOfLeds[3] = 1;
    }
    else
    {
        halCLR_LED( LED1 );
        ucStateOfLeds[3] = 0;
    }

    if ( QueueMailboxMsg.usAvgResult1 < usThreshold )
    {
        halSET_LED( LED2 );
        ucStateOfLeds[2] = 1;
    }
    else
    {
        halCLR_LED( LED2 );
        ucStateOfLeds[2] = 0;
    }

    if ( QueueMailboxMsg.usAvgResult2 < usThreshold )
    {
        halSET_LED( LED3 );
        ucStateOfLeds[1] = 1;
    }
    else
    {
        halCLR_LED( LED3 );
        ucStateOfLeds[1] = 0;
    }

    if ( QueueMailboxMsg.usAvgResult3 < usThreshold )
    {
        halSET_LED( LED4 );
        ucStateOfLeds[0] = 1;
    }
    else
    {
        halCLR_LED( LED4 );
        ucStateOfLeds[0] = 0;
    }
}

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/**
 * @brief Timer debounce callback
 */
static void vTimerDebounceCallback( TimerHandle_t xTimer )
{
    xSemaphoreGive( xSemaBDebounce );
}

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/**
 * @brief UART receive callback
 * @param ucData received character
 *
 * Function that is called every time a byte is received over UART.
 */
static void prvReceiveByteCallback( uint8_t ucData )
{
BaseType_t xHigherPriorityTaskWoken = pdFALSE;
/** number of received characters */
static uint8_t ucRxCnt = 0;
/** buffer where data received over UART is placed */
static uint8_t pucRxData[4];

    /* append received byte to buffer */
    pucRxData[ ucRxCnt++ ] = ASCII2DIGIT( ucData );
    /* when we have received 4 bytes, send massage to queue */
    if( ucRxCnt == 4 )
    {
        uint16_t usThresholdFromUart = ( pucRxData[0] * 1000 ) + ( pucRxData[1] * 100 ) + ( pucRxData[2] * 10 ) + pucRxData[3];
        QueueT2Msg_t xMsg = { sourceUARTCallback, usThresholdFromUart };
        xQueueSendToBackFromISR( xQueueT2, &xMsg, &xHigherPriorityTaskWoken );
        ucRxCnt = 0;
    }

    portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/**
 * @brief main function
 */
int main( void )
{
    /** 20 ms timer used for button debouncing */
    TimerHandle_t xTimerDebounce = NULL;

    /* Configure peripherals */
    prvSetupHardware();

    /* Configure UART */
    vUartInit();

    /* Install UART Callback routine */
    vUartSetRxCallback( prvReceiveByteCallback );

    /* Create tasks */
    xTaskCreate( prvTaskTimer, "TaskTimer", configMINIMAL_STACK_SIZE, NULL, mainHP_TASK_PRIO, NULL);
    xTaskCreate( prvTask1, "Task 1", configMINIMAL_STACK_SIZE, NULL, mainSP_TASK_PRIO, &xTask1 );
    xTaskCreate( prvTask2, "Task 2", configMINIMAL_STACK_SIZE, NULL, mainTP_TASK_PRIO, NULL );
    xTaskCreate( prvTask3, "Task 3", configMINIMAL_STACK_SIZE, NULL, mainFP_TASK_PRIO, NULL );
    xTaskCreate( prvTaskLEDs, "Task LEDs", configMINIMAL_STACK_SIZE, NULL, mainLED_TASK_PRIO, NULL );

    /* Create binary semaphore */
    xSemaBDebounce = xSemaphoreCreateBinary();

    /* Create queues */
    xADCDataQueue = xQueueCreate( adcADC_DATA_QUEUE_LEN, sizeof( ADCMsg_t ) );
    xQueueT2 = xQueueCreate( mainQUEUE_T2_LEN, sizeof( QueueT2Msg_t ) );
    xQueueMailbox = xQueueCreate( 1, sizeof( QueueMailboxMsg_t ) );

    /* Create timer and start it */
    xTimerDebounce = xTimerCreate( "TimerDebounce", mainTIMER_DEB_PERIOD, pdTRUE, NULL, vTimerDebounceCallback );
    xTimerStart( xTimerDebounce, 0 );

    /* Start the scheduler. */
    vTaskStartScheduler();

    /* If all is well then this line will never be reached.  If it is reached
    then it is likely that there was insufficient (FreeRTOS) heap memory space
    to create the idle task.  This may have been trapped by the malloc() failed
    hook function, if one is configured. */
    for( ;; );
}

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/**
 * @brief Configure hardware upon boot
 */
static void prvSetupHardware( void )
{
    taskDISABLE_INTERRUPTS();

    /* Disable the watchdog. */
    WDTCTL = WDTPW + WDTHOLD;

    /* Configure Clock. Since we aren't using XT1 on the board,
     * configure REFOCLK to source FLL and ACLK.
     */
    SELECT_FLLREF(SELREF__REFOCLK);
    SELECT_ACLK(SELA__REFOCLK);
    hal430SetSystemClock( configCPU_CLOCK_HZ, configLFXT_CLOCK_HZ );

    /* Enable buttons */
    P2DIR &= ~(0xF0);

    /* initialize LEDs */
    vHALInitLED();

    /* configuration of AD converter */

    /* initialize ADC */
    P7SEL |= BIT6 + BIT7;
    P5SEL |= BIT0 + BIT1;

    /* multiplexing of pins */
    ADC12CTL0 = ADC12ON | ADC12MSC;                         /* uses MSC */
    ADC12CTL1 = ADC12SHS_0 | ADC12CONSEQ_1 | ADC12SHP;      /* repeat-sequence, SC starts it */
    ADC12MCTL0 = ADC12INCH_14;
    ADC12MCTL1 = ADC12INCH_15;
    ADC12MCTL2 = ADC12INCH_8;
    ADC12MCTL3 = ADC12INCH_9 | ADC12EOS;                    /* MEM3 is end of sequence */
    ADC12IE |= ADC12IE3;                                    /* enable only last interrupt in sequence */
    ADC12CTL0 |= ADC12ENC;                                  /* enable conversion */
}

/*———————————————————–————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

/**
 * @brief ISR of ADC
 *
 * In ISR result of all 4 AD conversions are sent via queue "xADCDataQueue".
 */
void __attribute__ ( ( interrupt( ADC12_VECTOR ) ) ) vADCISR( void )
{
BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    switch( ADC12IV )
        {
        case 12:                    /* Vector 12: ADC12IFG3 */
        {
            ADCMsg_t xMsgCH0 = { CH0, ADC12MEM0 };
            xQueueSendToBackFromISR( xADCDataQueue, &xMsgCH0, &xHigherPriorityTaskWoken );

            ADCMsg_t xMsgCH1 = { CH1, ADC12MEM1 };
            xQueueSendToBackFromISR( xADCDataQueue, &xMsgCH1, &xHigherPriorityTaskWoken );

            ADCMsg_t xMsgCH2 = { CH2, ADC12MEM2 };
            xQueueSendToBackFromISR( xADCDataQueue, &xMsgCH2, &xHigherPriorityTaskWoken );

            ADCMsg_t xMsgCH3 = { CH3, ADC12MEM3 };
            xQueueSendToBackFromISR( xADCDataQueue, &xMsgCH3, &xHigherPriorityTaskWoken );

            ucCount += 4;

            vTaskNotifyGiveFromISR( xTask1, &xHigherPriorityTaskWoken );

            /* trigger scheduler if higher priority task is woken */
            portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
        }
        break;
        default: break;
        }
}
