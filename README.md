# FREE RTOS basics usage

The project was realized within the course Real-time Systems. The goal of the project is the acquisition from channel A14, A15, A8, A9 of the AD converter integrated on the development board RSMSP430F5438 and signaling of those LED diodes, which correspond to the channels on which the mean value is lower than the one set via UART. The program was written in the development environment Code Composer Studio 9.3.
